package main.java.com.ubo.tp.message.ihm.message;

import main.java.com.ubo.tp.message.ihm.message.models.FiltreMessageModel;
import main.java.com.ubo.tp.message.ihm.message.models.ListeMessageModel;
import main.java.com.ubo.tp.message.ihm.message.views.ListeMessageView;
import main.java.com.ubo.tp.message.ihm.message.views.MessageAffichageView;
import main.java.com.ubo.tp.message.ihm.message.views.MessageFiltreView;
import main.java.com.ubo.tp.message.ihm.message.views.MessageSaisieView;

public class MessageComponent {

    protected MessageController messageController;
    protected MessageSaisieView messageSaisieView;
    protected MessageAffichageView messageAffichageView;
    protected ListeMessageView listeMessageView;
    protected MessageFiltreView messageFiltreView;
    protected FiltreMessageModel filtreMessageModel;
    protected ListeMessageModel listeMessageModel;

    public MessageComponent(MessageController messageController, ListeMessageModel listeMessageModel, FiltreMessageModel filtreMessageModel) {
        this.messageController = messageController;

        // Models
        this.filtreMessageModel = filtreMessageModel;
        this.listeMessageModel = listeMessageModel;

        // Views
        this.messageSaisieView = new MessageSaisieView();
        this.messageAffichageView = new MessageAffichageView();
        this.messageFiltreView = new MessageFiltreView(filtreMessageModel);
        this.listeMessageView = new ListeMessageView(filtreMessageModel, listeMessageModel, messageFiltreView, messageAffichageView);

        this.messageFiltreView.addObserver(this.messageController);
        this.addMessageObserver(this.messageController);
    }

    public void addMessageObserver(MessageObserver messageObserver) {
        this.messageSaisieView.addObserver(messageObserver);
        this.messageAffichageView.addObserver(messageObserver);
    }

    public MessageSaisieView getMessageSaisieView() {
        return messageSaisieView;
    }

    public MessageAffichageView getMessageAffichageView() {
        return messageAffichageView;
    }

    public ListeMessageView getListeMessageView() {
        return listeMessageView;
    }

    public MessageFiltreView getMessageFiltreView() {
        return messageFiltreView;
    }
}
