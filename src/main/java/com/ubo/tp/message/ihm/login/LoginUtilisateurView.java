package main.java.com.ubo.tp.message.ihm.login;

import main.java.com.ubo.tp.message.datamodel.User;
import main.java.com.ubo.tp.message.ihm.login.models.UtilisateurModel;
import main.java.com.ubo.tp.message.ihm.session.Session;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class LoginUtilisateurView extends JPanel {
    protected List<ObserverLogin> observerList = new ArrayList<>();
    protected List<AbonnementObserver> abonnementObserverList = new ArrayList<>();
    protected List<User> listUser;
    protected UtilisateurModel utilisateurModel;
    protected Session session;

    public void addObserver(ObserverLogin o) {
        this.observerList.add(o);
    }
    public void addAbonnementObserver(AbonnementObserver o) {
        this.abonnementObserverList.add(o);
    }

    public LoginUtilisateurView(UtilisateurModel utilisateurModel, Session session) {
        this.utilisateurModel = utilisateurModel;
        this.session = session;
        afficherUtilisateur();
    }
    public void afficherUtilisateur() {
        this.removeAll();
        this.listUser = this.utilisateurModel.getUserList();
        this.setLayout(new GridBagLayout());
        JLabel dbLabel = new JLabel("Utilisateurs");

        JLabel jLabel1 =new JLabel("Recherche : ");
        jLabel1.setDisplayedMnemonic('n');

        JTextField jEdit = new JTextField();

        Button boutonRechercher = new Button("Rechercher");
        boutonRechercher.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("rechercher utilisateur:" + jEdit.getText());
                utilisateurModel.setFiltre(jEdit.getText());
            }
        });
        JPanel panelUtilisateurs = new JPanel(new GridBagLayout());
        if(this.listUser != null) {
            int i = 0;
            for (final User u : this.listUser) {

                JLabel labelNom = new JLabel(u.getName()+"@"+u.getUserTag()+ (session != null && session.getConnectedUser() != null && session.getConnectedUser().getFollows().contains(u.getUserTag())? "-Abonné-": ""));
                Button boutonAbonnement = new Button(session != null && session.getConnectedUser() != null && session.getConnectedUser().getFollows().contains(u.getUserTag())? "Se desabonner": "S'abonner");
                boutonAbonnement.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        for(AbonnementObserver o: abonnementObserverList) {
                            if (session != null && session.getConnectedUser() != null && session.getConnectedUser().getFollows().contains(u.getUserTag())) {
                                o.notifyDesabonnement(u.getUserTag());
                            } else {
                                o.notifyAbonnement(u.getUserTag());
                            }
                        }
                    }
                });
                panelUtilisateurs.add(labelNom, new GridBagConstraints(0, i, 1, 1, 1, 1, GridBagConstraints.WEST,
                        GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
                panelUtilisateurs.add(boutonAbonnement, new GridBagConstraints(1, i, 1, 1, 1, 1, GridBagConstraints.WEST,
                        GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
                i++;
            }
        }

        JScrollPane scrollPane = new JScrollPane(panelUtilisateurs);
        //
        // Ajout des composants à la fenêtre
        this.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(jEdit, new GridBagConstraints(1, 0, 1, 1, 4, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(boutonRechercher, new GridBagConstraints(2, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(scrollPane, new GridBagConstraints(1, 1, 1, 1, 4, 1, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));

        this.repaint();
        this.revalidate();
    }
}
