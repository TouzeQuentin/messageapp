package main.java.com.ubo.tp.message.ihm.message.views;

import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.ihm.message.MessageObserver;
import main.resources.ImagePanel;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageAffichageView extends JPanel {
    protected List<MessageObserver> observerList = new ArrayList<>();

    protected Message message;

    public void addObserver(MessageObserver o) {
        this.observerList.add(o);
    }

    public MessageAffichageView() {
        this.afficherMessage();
    }

    public void afficherMessage() {
        this.removeAll();
        if(message != null) {
            JPanel panel = new JPanel(new GridBagLayout());
            this.setLayout(new GridBagLayout());
            this.setBackground(Color.LIGHT_GRAY);
            panel.setBackground(Color.LIGHT_GRAY);
            Date date = new Date(message.getEmissionDate());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String formattedDate = dateFormat.format(date);
            if (message.getSender().getAvatarPath()!=null || !message.getSender().getAvatarPath().isEmpty()) {
                File file = new File(message.getSender().getAvatarPath());
                JPanel image = new ImagePanel(file, new Dimension(30, 30));
                panel.add(image, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST,
                        GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
            }
            JLabel dbLabel = new JLabel("<html><b>"+message.getSender().getName()+"@"+message.getSender().getUserTag()+"<b> (le "+ formattedDate +"):</html>");
            JLabel textLabel = new JLabel(message.getText());

            panel.add(dbLabel, new GridBagConstraints(2, 0, 1, 1, 20, 1, GridBagConstraints.NORTHWEST,
                    GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
            //
            // Ajout des composants à la fenêtre
            this.add(panel, new GridBagConstraints(0, 0, 2, 1, 1, 1, GridBagConstraints.NORTHWEST,
                    GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
            this.add(textLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.WEST,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        }
    }

    protected void setMessage(Message m) {
        this.message = m;
    }
}
