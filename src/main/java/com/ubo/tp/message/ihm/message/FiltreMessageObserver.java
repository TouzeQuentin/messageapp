package main.java.com.ubo.tp.message.ihm.message;

public interface FiltreMessageObserver {

    void notifyFiltreModifier();
}
