package main.java.com.ubo.tp.message.ihm.login;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginViewConnexion extends JPanel {

    protected List<ObserverLogin> observerList = new ArrayList<>();

    public void addObserver(ObserverLogin o) {
        this.observerList.add(o);
    }

    public LoginViewConnexion() {
        this.setLayout(new GridBagLayout());
        JLabel labelVide = new JLabel();
        JLabel labelVide2 = new JLabel();
        JPanel composantChamps = new JPanel(new GridBagLayout());
        JLabel dbLabel = new JLabel("Connexion");
        //
        JTextField jEdit = new JTextField();

        JLabel jLabel1 =new JLabel("Tag : ");
        jLabel1.setDisplayedMnemonic('n');

        JPasswordField  passwordField1 = new JPasswordField ("");

        JLabel jLabelMdp =new JLabel("Mot de passe : ");
        jLabelMdp.setDisplayedMnemonic('n');

        Button ajouterCompte = new Button("Connexion");
        ajouterCompte.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                boolean ok = true;
                if(jEdit.getText().equals("")) {
                    jEdit.setBackground(Color.RED);
                    ok=false;
                } else {
                    jEdit.setBackground(Color.WHITE);
                }

                if(ok){
                    LoginViewConnexion.this.connexionUtilisateur(jEdit.getText(), Arrays.toString(passwordField1.getPassword()));
                }
            }
        });
        //
        // Ajout des composants à la fenêtre

        composantChamps.add(jLabel1, new GridBagConstraints(0, 1, 1, 1, 0, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composantChamps.add(jEdit, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        composantChamps.add(jLabelMdp, new GridBagConstraints(0, 2, 1, 1, 0, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composantChamps.add(passwordField1, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        composantChamps.add(ajouterCompte, new GridBagConstraints(0, 3, 2, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));

        this.add(labelVide, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(dbLabel, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(composantChamps, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
        this.add(labelVide2, new GridBagConstraints(2, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    }

    protected void connexionUtilisateur(String tag, String mdp) {
        for(ObserverLogin o: this.observerList) {
            o.notifyConnexion(tag, mdp);
        }
    }
}
