package main.java.com.ubo.tp.message.ihm.message;

import main.java.com.ubo.tp.message.datamodel.Message;

public interface MessageObserver {

    void notifyEnvoyerMessage(String msg);

    Message notifyGetMessage();
}
