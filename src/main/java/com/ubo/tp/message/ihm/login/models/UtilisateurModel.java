package main.java.com.ubo.tp.message.ihm.login.models;

import main.java.com.ubo.tp.message.datamodel.User;
import main.java.com.ubo.tp.message.ihm.login.UtilisateurFiltreObserver;

import java.util.ArrayList;
import java.util.List;

public class UtilisateurModel {
    protected List<UtilisateurFiltreObserver> observer = new ArrayList<>();
    List<User> userList;
    String filtre;

    public void addObserver(UtilisateurFiltreObserver o) {
        observer.add(o);
    }
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getFiltre() {
        return filtre;
    }

    public void setFiltre(String filtre) {
        this.filtre = filtre;
        for(UtilisateurFiltreObserver o: observer) {
            o.notifyFiltreModifier();
        }
    }
}
