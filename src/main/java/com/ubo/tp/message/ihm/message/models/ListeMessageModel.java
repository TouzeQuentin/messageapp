package main.java.com.ubo.tp.message.ihm.message.models;

import main.java.com.ubo.tp.message.datamodel.Message;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ListeMessageModel {
    protected List<Message> listeMessages = new ArrayList<>();
    public void setMessages(List<Message> listeMessages) {

        listeMessages.sort(new Comparator<Message>() {
            @Override
            public int compare(Message message1, Message message2) {
                return Long.compare(message2.getEmissionDate(), message1.getEmissionDate());
            }
        });
        this.listeMessages = listeMessages;
    }
    public List<Message> getMessages() {
        return listeMessages;
    }
}
