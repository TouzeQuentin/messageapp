package main.java.com.ubo.tp.message.ihm.message.models;

import main.java.com.ubo.tp.message.ihm.message.FiltreMessageObserver;

import java.util.ArrayList;
import java.util.List;

public class FiltreMessageModel {
    protected List<FiltreMessageObserver> observer = new ArrayList<>();
    protected String filtre;

    public void addObserver(FiltreMessageObserver o) {
        observer.add(o);
    }

    public String getFiltre() {
        return filtre;
    }

    public void setFiltre(String filtre) {
        this.filtre = filtre;
        for (FiltreMessageObserver o: observer) {
            o.notifyFiltreModifier();
        }
    }
}
