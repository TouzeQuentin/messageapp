package main.java.com.ubo.tp.message.ihm.login;

import main.java.com.ubo.tp.message.datamodel.User;
import main.resources.ImagePanel;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LoginProfilView extends JPanel {
    protected List<ObserverLogin> observerList = new ArrayList<>();
    protected User user;

    public void addObserver(ObserverLogin o) {
        this.observerList.add(o);
    }

    public void trouverUser(UUID id) {
        for (ObserverLogin o:this.observerList) {
            this.user = o.notifyGetUser(id);
        }
    }
    public LoginProfilView() {
        afficherProfil();
    }
    public void afficherProfil() {
        this.removeAll();
        this.setLayout(new GridBagLayout());
        JLabel dbLabel = new JLabel("Profil");
        //

        JLabel labelNom =new JLabel("Nom : ");
        labelNom.setDisplayedMnemonic('n');
        JLabel labelResNom =new JLabel(user != null? user.getName() : "");
        labelResNom.setDisplayedMnemonic('n');

        JLabel labelTag =new JLabel("Tag : ");
        labelTag.setDisplayedMnemonic('n');
        JLabel labelResTag =new JLabel(user != null? user.getUserTag() : "");
        labelResTag.setDisplayedMnemonic('n');


        //
        // Ajout des composants à la fenêtre
        this.add(dbLabel, new GridBagConstraints(0, 0, 2, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        if (user!=null) {
            File file = new File(user.getAvatarPath());
            JPanel image = new ImagePanel(file, new Dimension(200, 200));
            this.add(image, new GridBagConstraints(0, 1, 2, 1, 1, 1, GridBagConstraints.CENTER,
                    GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        }
        this.add(labelNom, new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(labelResNom, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(labelTag, new GridBagConstraints(0, 3, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(labelResTag, new GridBagConstraints(1, 3, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));

    }
}
