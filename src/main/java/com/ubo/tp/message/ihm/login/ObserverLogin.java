package main.java.com.ubo.tp.message.ihm.login;

import main.java.com.ubo.tp.message.datamodel.User;

import java.util.UUID;

public interface ObserverLogin {

    void notifyAddUser(String nom, String tag, String mdp, String avatar);

    void notifyConnexion(String tag, String mdp);

    void notifyDeconnexion();

    User notifyGetUser(UUID id);
}
