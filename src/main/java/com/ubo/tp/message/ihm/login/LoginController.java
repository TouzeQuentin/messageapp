package main.java.com.ubo.tp.message.ihm.login;

import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.datamodel.User;
import main.java.com.ubo.tp.message.ihm.MessageApp;
import main.java.com.ubo.tp.message.ihm.Navigation;
import main.java.com.ubo.tp.message.ihm.ObserverMsg;
import main.java.com.ubo.tp.message.ihm.login.models.UtilisateurModel;
import main.java.com.ubo.tp.message.ihm.session.Session;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class LoginController implements ObserverLogin, UtilisateurFiltreObserver, AbonnementObserver {

    IDatabase mDatabase;
    EntityManager mEntityManager;
    protected Session session;
    protected List<ObserverMsg> observerMsgList;
    protected UtilisateurModel utilisateurModel;
    protected LoginUtilisateurView loginUtilisateurView;
    protected Navigation navigation;

    public LoginController(IDatabase database, EntityManager entityManager, Session session, MessageApp messageApp, UtilisateurModel utilisateurModel, Navigation navigation) {
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.session = session;
        this.observerMsgList = new ArrayList<>();
        this.utilisateurModel = utilisateurModel;
        this.addObserverMsg(messageApp);
        this.utilisateurModel.addObserver(this);
        this.navigation = navigation;

        this.mettreAJourUsers();
    }

    public void addObserverMsg(ObserverMsg o) {
        this.observerMsgList.add(o);
    }

    @Override
    public void notifyAddUser(String nom, String tag, String mdp, String avatar) {
        boolean ok = true;
        User usr = new User(UUID.randomUUID(), tag, mdp, nom, new HashSet<>(), avatar);
        for(User u :this.mDatabase.getUsers()) {
            if(u.getUserTag().equals(usr.getUserTag())) {
                ok= false;
                break;
            }
        }

        if(ok) {
            this.mEntityManager.writeUserFile(usr);
            for(ObserverMsg o: this.observerMsgList) {
                o.notifyAfficherMessageEnregistrementOk();
            }
            this.navigation.naviguerVersConnexion();
        } else {
            for(ObserverMsg o: this.observerMsgList) {
                o.notifyAfficherMessageTagExistant();
            }
        }
    }

    @Override
    public void notifyConnexion(String tag, String mdp) {
        List<User> a = this.mDatabase.getUsers().stream().filter(user -> user.getUserTag().equals(tag) && user.getUserPassword().equals(mdp)).collect(Collectors.toList());
        if(a.isEmpty()) {
            for(ObserverMsg o: this.observerMsgList) {
                o.notifyAfficherMessageConnexionKo();
            }
        } else {
            User usr = a.get(0);
            this.session.connect(usr);
            System.out.println("Connexion OK :" + usr);
            for(ObserverMsg o: this.observerMsgList) {
                o.notifyAfficherMessageConnexionOk();
            }
            this.navigation.naviguerVersListeMessages();
        }
    }

    @Override
    public void notifyDeconnexion() {

    }

    @Override
    public User notifyGetUser(UUID id) {
        return id != null?this.mDatabase.getUsers().stream().filter(user -> user.getUuid().equals(id)).collect(Collectors.toList()).get(0): this.session.getConnectedUser();
    }

    public void deconnexion() {
        this.session.disconnect();
        for(ObserverMsg o: this.observerMsgList) {
            o.notifyAfficherMessageDeconnexionOk();
        }
    }

    @Override
    public void notifyFiltreModifier() {
        this.mettreAJourUsers();
    }
    public void mettreAJourUsers() {
        String filtre = this.utilisateurModel.getFiltre() != null? this.utilisateurModel.getFiltre().toLowerCase(): "";
        this.utilisateurModel.setUserList(this.mDatabase.getUsers().stream().filter(user -> {
            return user.getUserTag().toLowerCase().contains(filtre) || user.getName().toLowerCase().contains(filtre);
        }).collect(Collectors.toList()));
        if(loginUtilisateurView != null) {
            this.loginUtilisateurView.afficherUtilisateur();
        }
    }

    public LoginUtilisateurView getLoginUtilisateurView() {
        return loginUtilisateurView;
    }

    public void setLoginUtilisateurView(LoginUtilisateurView loginUtilisateurView) {
        this.loginUtilisateurView = loginUtilisateurView;
    }

    @Override
    public void notifyAbonnement(String user) {
        User u = this.session.getConnectedUser();
        u.addFollowing(user);
        this.mEntityManager.writeUserFile(u);
        this.mettreAJourUsers();
    }

    @Override
    public void notifyDesabonnement(String user) {
        User u = this.session.getConnectedUser();
        u.removeFollowing(user);
        this.mEntityManager.writeUserFile(u);
        this.mettreAJourUsers();
    }


}
