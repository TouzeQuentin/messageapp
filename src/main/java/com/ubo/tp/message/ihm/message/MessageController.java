package main.java.com.ubo.tp.message.ihm.message;

import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.core.database.IDatabaseObserver;
import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.datamodel.User;
import main.java.com.ubo.tp.message.ihm.Navigation;
import main.java.com.ubo.tp.message.ihm.Notification;
import main.java.com.ubo.tp.message.ihm.message.models.FiltreMessageModel;
import main.java.com.ubo.tp.message.ihm.message.models.ListeMessageModel;
import main.java.com.ubo.tp.message.ihm.message.views.ListeMessageView;
import main.java.com.ubo.tp.message.ihm.session.Session;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MessageController implements MessageObserver, IDatabaseObserver, FiltreMessageObserver {
    /**
     * Base de données.
     */
    protected IDatabase mDatabase;

    /**
     * Gestionnaire des entités contenu de la base de données.
     */
    protected EntityManager mEntityManager;
    protected Session session;
    protected ListeMessageModel listeMessageModel;
    protected FiltreMessageModel filtreMessageModel;
    protected ListeMessageView listeMessageView;
    protected Notification notification;
    protected Navigation navigation;

    public MessageController(IDatabase database, EntityManager entityManager, Session session, ListeMessageModel listeMessageModel, FiltreMessageModel filtreMessageModel, Notification notification, Navigation navigation) {
        this.mDatabase = database;
        this.mEntityManager = entityManager;
        this.session = session;
        this.listeMessageModel = listeMessageModel;
        this.filtreMessageModel = filtreMessageModel;
        this.notification = notification;
        this.navigation = navigation;

        this.filtreMessageModel.addObserver(this);
        this.mDatabase.addObserver(this);
        this.mettreAJourMessages();
    }

    public void mettreAJourMessages() {
        this.listeMessageModel.setMessages(new ArrayList<>(this.mDatabase.getMessages()).stream().filter(message -> {
            String filtre = this.filtreMessageModel.getFiltre();

            if(filtre == null || filtre.isEmpty()) {
                return true;
            }
            if(filtre.contains("@") && !filtre.contains(" ")) {
                return message.getSender().getUserTag().contains(filtre.substring(1).toLowerCase()) || message.containsUserTag(filtre.substring(1).toLowerCase());
            }
            if(filtre.contains("#") && !filtre.contains(" ")) {
                return message.containsTag(filtre.substring(1).toLowerCase());
            }

            return message.getText().toLowerCase().contains(filtre.toLowerCase()) || message.getSender().getUserTag().contains(filtre.substring(1).toLowerCase());
        }).collect(Collectors.toList()));
        if (listeMessageView != null) {
            listeMessageView.afficherMessage();
        }
    }

    public void envoyerNotification(Message msg) {
        if(this.session.getConnectedUser() != null && this.session.getConnectedUser().getFollows() != null) {
            if (this.session.getConnectedUser().getFollows().contains(msg.getSender().getUserTag())) {
                notification.notifier("Nouveau message de '" + msg.getSender().getName() + "@" + msg.getSender().getUserTag() + "' :", msg.getText());
            }
        }
    }

    @Override
    public void notifyEnvoyerMessage(String msg) {
        User usr = this.session.getConnectedUser();
        if(usr != null) {
            Message message = new Message(usr, msg);
            this.mEntityManager.writeMessageFile(message);
            this.navigation.naviguerVersListeMessages();
        } else {
            System.out.println("pas user");
        }
    }

    @Override
    public Message notifyGetMessage() {
        return new ArrayList<>(this.mDatabase.getMessages()).get(0);
    }

    @Override
    public void notifyMessageAdded(Message addedMessage) {
        this.mettreAJourMessages();
        this.envoyerNotification(addedMessage);
    }

    @Override
    public void notifyMessageDeleted(Message deletedMessage) {
        this.mettreAJourMessages();
    }

    @Override
    public void notifyMessageModified(Message modifiedMessage) {
        this.mettreAJourMessages();
    }

    @Override
    public void notifyUserAdded(User addedUser) {

    }

    @Override
    public void notifyUserDeleted(User deletedUser) {

    }

    @Override
    public void notifyUserModified(User modifiedUser) {

    }

    @Override
    public void notifyFiltreModifier() {
        this.mettreAJourMessages();
    }

    public ListeMessageView getListeMessageView() {
        return listeMessageView;
    }

    public void setListeMessageView(ListeMessageView listeMessageView) {
        this.listeMessageView = listeMessageView;
    }
}
