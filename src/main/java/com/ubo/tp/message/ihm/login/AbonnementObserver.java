package main.java.com.ubo.tp.message.ihm.login;

import main.java.com.ubo.tp.message.datamodel.User;

public interface AbonnementObserver {

    void notifyAbonnement(String user);
    void notifyDesabonnement(String user);
}
