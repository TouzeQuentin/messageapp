package main.java.com.ubo.tp.message.ihm;

import java.io.File;

import main.java.com.ubo.tp.message.core.EntityManager;
import main.java.com.ubo.tp.message.core.database.IDatabase;
import main.java.com.ubo.tp.message.core.directory.IWatchableDirectory;
import main.java.com.ubo.tp.message.core.directory.WatchableDirectory;
import main.java.com.ubo.tp.message.datamodel.User;
import main.java.com.ubo.tp.message.ihm.login.LoginComponent;
import main.java.com.ubo.tp.message.ihm.login.LoginController;
import main.java.com.ubo.tp.message.ihm.login.models.UtilisateurModel;
import main.java.com.ubo.tp.message.ihm.message.MessageComponent;
import main.java.com.ubo.tp.message.ihm.message.MessageController;
import main.java.com.ubo.tp.message.ihm.message.models.FiltreMessageModel;
import main.java.com.ubo.tp.message.ihm.message.models.ListeMessageModel;
import main.java.com.ubo.tp.message.ihm.session.ISessionObserver;
import main.java.com.ubo.tp.message.ihm.session.Session;

import javax.swing.*;

/**
 * Classe principale l'application.
 *
 * @author S.Lucas
 */
public class MessageApp implements ObserverMsg, ISessionObserver {
	/**
	 * Base de données.
	 */
	protected IDatabase mDatabase;

	/**
	 * Gestionnaire des entités contenu de la base de données.
	 */
	protected EntityManager mEntityManager;

	/**
	 * Vue principale de l'application.
	 */
	protected MessageAppMainView mMainView;

	/**
	 * Classe de surveillance de répertoire
	 */
	protected IWatchableDirectory mWatchableDirectory;

	/**
	 * Répertoire d'échange de l'application.
	 */
	protected String mExchangeDirectoryPath;

	/**
	 * Nom de la classe de l'UI.
	 */
	protected String mUiClassName;
	protected LoginComponent loginComponent;
	protected LoginController loginController;
	protected MessageController messageController;
	protected MessageComponent messageComponent;
	protected Session session;
	protected Navigation navigation;
	protected JPanel panel;
	protected ListeMessageModel listeMessageModel;
	protected FiltreMessageModel filtreMessageModel;
	protected Notification notification;
	protected UtilisateurModel utilisateurModel;

	/**
	 * Constructeur.
	 *
	 * @param entityManager
	 * @param database
	 */
	public MessageApp(IDatabase database, EntityManager entityManager) {
		this.mDatabase = database;
		this.mEntityManager = entityManager;

		this.session = new Session();
		this.notification = new Notification();
		navigation = new Navigation(this.session);
		this.mMainView = new MessageAppMainView(navigation);

	}

	/**
	 * Initialisation de l'application.
	 */
	public void init() {
		// Init du look and feel de l'application
		this.initLookAndFeel();

		// Initialisation du répertoire d'échange
		this.initDirectory();

		this.initGui();

		this.panel = this.mMainView.panel;
		navigation.setPanel(panel);


		this.listeMessageModel = new ListeMessageModel();
		this.filtreMessageModel = new FiltreMessageModel();
		this.utilisateurModel = new UtilisateurModel();

		this.loginController = new LoginController(this.mDatabase, this.mEntityManager, this.session, this, utilisateurModel, navigation);
		this.loginComponent = new LoginComponent(this.loginController, this.mMainView.panel, utilisateurModel, session);

		this.messageController = new MessageController(mDatabase, mEntityManager, session, listeMessageModel, filtreMessageModel, notification, navigation);
		this.messageComponent = new MessageComponent(messageController, listeMessageModel, filtreMessageModel);

		this.messageController.setListeMessageView(messageComponent.getListeMessageView());
		// Navigation
		navigation.setLoginViewEnregistrer(loginComponent.getLoginViewEnregistrer());
		navigation.setLoginViewConnexion(loginComponent.getLoginViewConnexion());
		navigation.setMessageSaisieView(messageComponent.getMessageSaisieView());
		navigation.setMessageAffichageView(messageComponent.getMessageAffichageView());
		navigation.setLoginProfilView(loginComponent.getLoginProfilView());
		navigation.setLoginUtilisateurView(loginComponent.getLoginUtilisateurView());
		navigation.setListeMessageView(messageComponent.getListeMessageView());

		this.loginComponent.addObserverMsg(this);

		this.mMainView.addObserver(this.loginController);

		this.navigation.naviguerVersConnexion();
	}

	/**
	 * Initialisation du look and feel de l'application.
	 */
	protected void initLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialisation de l'interface graphique.
	 */
	protected void initGui() {
		this.mMainView.showGUI();
	}

	/**
	 * Initialisation du répertoire d'échange (depuis la conf ou depuis un file
	 * chooser). <br/>
	 * <b>Le chemin doit obligatoirement avoir été saisi et être valide avant de
	 * pouvoir utiliser l'application</b>
	 */
	protected void initDirectory() {
		File fichier;
		do {
			fichier = this.mMainView.afficherSelectionFichier();
			if(fichier == null || !isValideExchangeDirectory(fichier))  {
				if(this.mMainView.afficherErreurFichier() == 0) {
					this.mMainView.quitter();
					return;
				}
			}
		} while(fichier == null);
		this.initDirectory(fichier.getAbsolutePath());
	}

	/**
	 * Indique si le fichier donné est valide pour servir de répertoire d'échange
	 *
	 * @param directory , Répertoire à tester.
	 */
	protected boolean isValideExchangeDirectory(File directory) {
		// Valide si répertoire disponible en lecture et écriture
		return directory != null && directory.exists() && directory.isDirectory() && directory.canRead()
				&& directory.canWrite();
	}

	/**
	 * Initialisation du répertoire d'échange.
	 *
	 * @param directoryPath
	 */
	protected void initDirectory(String directoryPath) {
		mExchangeDirectoryPath = directoryPath;
		mWatchableDirectory = new WatchableDirectory(directoryPath);
		mEntityManager.setExchangeDirectory(directoryPath);

		mWatchableDirectory.initWatching();
		mWatchableDirectory.addObserver(mEntityManager);
	}

	public void show() {
		// ... setVisible?
	}

	@Override
	public void notifyAfficherMessageTagExistant() {
		this.mMainView.afficherMessageTagExistant();
	}

	@Override
	public void notifyAfficherMessageEnregistrementOk() {
		this.mMainView.afficherMessageEnregistrementOk();
	}

	@Override
	public void notifyAfficherMessageConnexionOk() {
		this.mMainView.afficherMessageConnexionOk();
	}

	@Override
	public void notifyAfficherMessageConnexionKo() {
		this.mMainView.afficherMessageConnexionKo();
	}

	@Override
	public void notifyAfficherMessageDeconnexionOk() {
		this.mMainView.afficherMessageDeconnexionOk();
	}

	@Override
	public void notifyLogin(User connectedUser) {
	}

	@Override
	public void notifyLogout() {
	}
}
