package main.java.com.ubo.tp.message.ihm;

import main.java.com.ubo.tp.message.ihm.login.ObserverLogin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de la vue principale de l'application.
 */
public class MessageAppMainView {

    protected JFrame mFrame;
    protected JPanel panel;
    protected Navigation navigation;
    protected List<ObserverLogin> observerList = new ArrayList<>();

    public void addObserver(ObserverLogin o) {
        this.observerList.add(o);
    }
    public MessageAppMainView(Navigation nav) {
        this.navigation = nav;
    }
    /**
     * Lance l'afficahge de l'IHM.
     */
    public void showGUI() {

        // Init auto de l'IHM au cas ou ;)
        if (mFrame == null) {
            this.initGUI();
        }

        // Affichage dans l'EDT
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Custom de l'affichage
                JFrame frame = MessageAppMainView.this.mFrame;
                Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
                frame.setLocation((screenSize.width - frame.getWidth()) / 6,
                        (screenSize.height - frame.getHeight()) / 4);

                // Affichage
                MessageAppMainView.this.mFrame.setVisible(true);

                MessageAppMainView.this.mFrame.pack();
            }
        });
    }

    /**
     * Initialisation de l'IHM "A propos"
     */
    protected void initGUI() {
        // Création de la fenetre principale
        mFrame = new JFrame("MessageApp");
        Image icon = Toolkit.getDefaultToolkit().getImage("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\logo_20.png");
        mFrame.setIconImage(icon);

        this.panel = new JPanel(new BorderLayout());


        // Menu bar
        JMenuBar menuBar = new JMenuBar();
        JMenu menuFichier = new JMenu("Fichier");

        menuBar.add(menuFichier);
        JMenuItem item = new JMenuItem("Quitter");
        item.setIcon(new ImageIcon("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\exitIcon_20.png"));
        item.setToolTipText("Fermer l'application");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                mFrame.dispose();
            }
        });
        menuFichier.add(item);
        mFrame.setJMenuBar(menuBar);
        menuFichier = new JMenu("Compte");

        menuBar.add(menuFichier);
        item = new JMenuItem("Enregister compte");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                MessageAppMainView.this.navigation.naviguerVersEnregistrement();
            }
        });
        menuFichier.add(item);

        item = new JMenuItem("Voir profil");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                MessageAppMainView.this.navigation.naviguerVersProfil( null);
            }

        });
        menuFichier.add(item);

        item = new JMenuItem("Voir les utilisateurs");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                MessageAppMainView.this.navigation.naviguerVersUtilisateurs();
            }

        });
        menuFichier.add(item);

        item = new JMenuItem("Connexion");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                MessageAppMainView.this.navigation.naviguerVersConnexion();
            }

        });
        menuFichier.add(item);

        item = new JMenuItem("Déconnexion");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                for(ObserverLogin o: MessageAppMainView.this.observerList) {
                    o.notifyDeconnexion();
                }
                MessageAppMainView.this.navigation.naviguerVersConnexion();
            }

        });
        menuFichier.add(item);

        menuFichier = new JMenu("Message");

        item = new JMenuItem("Voir les messages");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                MessageAppMainView.this.navigation.naviguerVersListeMessages();
            }

        });
        menuFichier.add(item);

        menuBar.add(menuFichier);
        item = new JMenuItem("Saisie message");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                MessageAppMainView.this.navigation.naviguerVersSaisieMessage();
            }
        });
        menuFichier.add(item);

        menuFichier = new JMenu("?");

        menuBar.add(menuFichier);
        item = new JMenuItem("A propos");
        item.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                JOptionPane.showMessageDialog(mFrame, "<html><p style='text-align: center'>UBO M2 TIIL<br>Département Informatique</p></html>","A propos", 0, new ImageIcon("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\logo_50.png"));
            }
        });
        menuFichier.add(item);
        mFrame.setJMenuBar(menuBar);
        mFrame.getContentPane().add(panel);
    }

    protected File afficherSelectionFichier() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(mFrame);
        if(returnVal == JFileChooser.APPROVE_OPTION){
            return chooser.getSelectedFile();
        }
        return null;
    }

    protected int afficherErreurFichier() {
        return JOptionPane.showConfirmDialog(mFrame, "<html><p style='text-align: center'>La sélection d'un répertoire est obligatoire.<br> Voulez-vous quitter ?</p></html>","Information", 0);
    }

    protected void quitter() {
        mFrame.setVisible(false);
    }

    public void afficherMessageTagExistant() {
        JOptionPane.showMessageDialog(mFrame, "<html><p style='text-align: center'>Le tag existe déjà</p></html>","Attention", 0);
    }
    public void afficherMessageEnregistrementOk() {
        JOptionPane.showMessageDialog(mFrame, "<html><p style='text-align: center'>Le compte à était créé.</p></html>","Enregistrement", 0, new ImageIcon("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\logo_50.png"));
    }
    public void afficherMessageConnexionOk() {
        JOptionPane.showMessageDialog(mFrame, "<html><p style='text-align: center'>Vous êtes connecté.</p></html>","Connexion", 0, new ImageIcon("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\logo_50.png"));
    }
    public void afficherMessageConnexionKo() {
        JOptionPane.showMessageDialog(mFrame, "<html><p style='text-align: center'>Le tag ou le mot de passe est incorrect.</p></html>","Attention", 0);
    }
    public void afficherMessageDeconnexionOk() {
        JOptionPane.showMessageDialog(mFrame, "<html><p style='text-align: center'>Vous êtes déconnecté.</p></html>","Déconnexion", 0, new ImageIcon("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\logo_50.png"));
    }
}
