package main.java.com.ubo.tp.message.ihm;

import javax.swing.*;
import java.awt.*;

public class Notification {
    TrayIcon trayIcon;
    public Notification() {
        final PopupMenu popup = new PopupMenu();
        trayIcon =
                new TrayIcon(new ImageIcon("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\editIcon_20.png").getImage());
        final SystemTray tray = SystemTray.getSystemTray();

        trayIcon.setPopupMenu(popup);

        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
        }
    }

    public void notifier(String titre, String texte) {
        trayIcon.displayMessage(titre, texte, TrayIcon.MessageType.INFO);
    }
}
