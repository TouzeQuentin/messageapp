package main.java.com.ubo.tp.message.ihm.message.views;

import main.java.com.ubo.tp.message.ihm.message.MessageObserver;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class MessageSaisieView extends JPanel {

    protected List<MessageObserver> observerList = new ArrayList<>();

    public void addObserver(MessageObserver o) {
        this.observerList.add(o);
    }

    public MessageSaisieView() {
        this.setLayout(new GridBagLayout());
        JPanel panelVide1 = new JPanel();
        JPanel panelVide2 = new JPanel();
        JLabel dbLabel = new JLabel("Message");
        //
        JTextArea textArea = new JTextArea("");

        textArea.setFont(textArea.getFont().deriveFont(15.0F));
        JLabel labelErreur = new JLabel("");
        labelErreur.setForeground(Color.RED);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        textArea.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        Button envoyerMessage = new Button("Envoyer");
        envoyerMessage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                if(textArea.getText().length() >200) {
                    Border border = BorderFactory.createLineBorder(Color.RED);
                    textArea.setBorder(BorderFactory.createCompoundBorder(border,
                            BorderFactory.createEmptyBorder(10, 10, 10, 10)));

                    labelErreur.setText("Le message ne doit pas depasser 200 caracteres.");
                } else {
                    MessageSaisieView.this.envoyerMessage(textArea.getText());
                    labelErreur.setText("");
                }
            }
        });
        //
        // Ajout des composants à la fenêtre
        this.add(panelVide1, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(dbLabel, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        this.add(textArea, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(labelErreur, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(envoyerMessage, new GridBagConstraints(1, 3, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(panelVide2, new GridBagConstraints(2, 3, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
    }

    protected void envoyerMessage(String msg) {
        for(MessageObserver o: this.observerList) {
            o.notifyEnvoyerMessage(msg);
        }
    }
}
