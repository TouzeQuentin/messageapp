package main.java.com.ubo.tp.message.ihm.message.views;

import main.java.com.ubo.tp.message.ihm.message.FiltreMessageObserver;
import main.java.com.ubo.tp.message.ihm.message.models.FiltreMessageModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class MessageFiltreView extends JPanel {
    protected FiltreMessageModel filtreModel;
    protected List<FiltreMessageObserver> observerList = new ArrayList<>();

    public void addObserver(FiltreMessageObserver o) {
        this.observerList.add(o);
    }

    public MessageFiltreView(FiltreMessageModel filtreModel) {
        this.filtreModel = filtreModel;
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.LIGHT_GRAY);

        JTextField jEdit = new JTextField();

        Button boutonRechercher = new Button("Rechercher");
        boutonRechercher.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.out.println("rechercher :" + jEdit.getText());
                filtreModel.setFiltre(jEdit.getText());
            }
        });

        // Ajout des composants à la fenêtre
        this.add(jEdit, new GridBagConstraints(0, 0, 1, 1, 3, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(boutonRechercher, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
    }
}
