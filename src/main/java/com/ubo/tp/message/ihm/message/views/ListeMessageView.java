package main.java.com.ubo.tp.message.ihm.message.views;

import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.ihm.message.models.FiltreMessageModel;
import main.java.com.ubo.tp.message.ihm.message.models.ListeMessageModel;

import javax.swing.*;
import java.awt.*;

public class ListeMessageView extends JPanel {

    protected FiltreMessageModel filtreMessageModel;
    protected ListeMessageModel listeMessageModel;
    protected MessageFiltreView messageFiltreView;
    protected MessageAffichageView messageAffichageView;

    public ListeMessageView(
            FiltreMessageModel filtreMessageModel,
            ListeMessageModel listeMessageModel,
            MessageFiltreView messageFiltreView,
            MessageAffichageView messageAffichageView) {
        this.filtreMessageModel = filtreMessageModel;
        this.listeMessageModel = listeMessageModel;
        this.messageFiltreView = messageFiltreView;
        this.messageAffichageView = messageAffichageView;

        this.afficherMessage();
    }

    public void afficherMessage() {
        this.removeAll();
        this.setLayout(new GridBagLayout());
        JPanel messages = new JPanel(new GridBagLayout());

        int i = 0;
        for(Message m: listeMessageModel.getMessages()) {
            MessageAffichageView aff = new MessageAffichageView();
            aff.setMessage(m);
            aff.afficherMessage();
            messages.add(aff, new GridBagConstraints(0, i, 1, 1, 1, 1, GridBagConstraints.NORTH,
                    GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
            i++;
        }

        JScrollPane pane = new JScrollPane(messages);
        this.add(messageFiltreView, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        this.add(pane, new GridBagConstraints(0, 1, 1, 1, 1, 10, GridBagConstraints.NORTH,
                GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));

        this.repaint();
        this.revalidate();
    }
}
