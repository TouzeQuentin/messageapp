package main.java.com.ubo.tp.message.ihm.login;

import main.java.com.ubo.tp.message.ihm.ObserverMsg;
import main.java.com.ubo.tp.message.ihm.login.models.UtilisateurModel;
import main.java.com.ubo.tp.message.ihm.session.Session;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class LoginComponent {
    protected JPanel panel;
    protected List<ObserverLogin> observerList;
    protected List<ObserverMsg> observerMsgList;
    protected LoginViewEnregistrer loginViewEnregistrer;
    protected LoginViewConnexion loginViewConnexion;
    protected LoginProfilView loginProfilView;
    protected LoginController loginController;
    protected LoginUtilisateurView loginUtilisateurView;
    protected UtilisateurModel utilisateurModel;
    public LoginComponent(LoginController loginController, JPanel panel, UtilisateurModel utilisateurModel, Session session) {
        this.loginController = loginController;
        this.utilisateurModel = utilisateurModel;
        this.observerList = new ArrayList<>();
        this.observerMsgList = new ArrayList<>();
        this.loginViewEnregistrer = new LoginViewEnregistrer();
        this.loginViewConnexion = new LoginViewConnexion();
        this.loginProfilView = new LoginProfilView();
        this.loginUtilisateurView = new LoginUtilisateurView(utilisateurModel, session);
        this.panel = panel;

        this.loginController.setLoginUtilisateurView(this.loginUtilisateurView);
        this.addObservable(loginController);
        this.loginUtilisateurView.addAbonnementObserver(loginController);
    }

    public void addObservable(ObserverLogin o) {
        this.observerList.add(o);
        this.loginViewEnregistrer.addObserver(o);
        this.loginViewConnexion.addObserver(o);
        this.loginProfilView.addObserver(o);
        this.loginUtilisateurView.addObserver(o);
    }

    public void addObserverMsg(ObserverMsg o) {
    }

    public LoginViewEnregistrer getLoginViewEnregistrer() {
        return loginViewEnregistrer;
    }

    public LoginViewConnexion getLoginViewConnexion() {
        return loginViewConnexion;
    }

    public LoginProfilView getLoginProfilView() {
        return loginProfilView;
    }

    public LoginUtilisateurView getLoginUtilisateurView() {
        return loginUtilisateurView;
    }
}
