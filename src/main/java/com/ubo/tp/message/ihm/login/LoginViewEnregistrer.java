package main.java.com.ubo.tp.message.ihm.login;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.List;

public class LoginViewEnregistrer extends JPanel {

    protected List<ObserverLogin> observerList = new ArrayList<>();

    public void addObserver(ObserverLogin o) {
        this.observerList.add(o);
    }
    public LoginViewEnregistrer() {
        final String[] avatar = {""};
        this.setLayout(new GridBagLayout());
        JPanel labelVide = new JPanel();
        JPanel labelVide2 = new JPanel();
        JPanel composant = new JPanel(new GridBagLayout());

        JLabel titreLabel = new JLabel("Enregistrer un compte");

        JTextField jEdit = new JTextField();

        JLabel jLabel1 =new JLabel("Nom : ");
        jLabel1.setDisplayedMnemonic('n');

        JTextField jEditTag = new JTextField();

        JLabel jLabelTag =new JLabel("Tag : ");
        jLabelTag.setDisplayedMnemonic('n');

        JPasswordField  passwordField1 = new JPasswordField ("");

        JLabel jLabelMdp =new JLabel("Mot de passe : ");
        jLabelTag.setDisplayedMnemonic('n');

        JLabel jLabelImg = new JLabel("Avatar :");

        JButton choisirImageBtn = new JButton("Choisir un avatar");
        choisirImageBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                avatar[0] = choisirImage();
            }
        });

        Button ajouterCompte = new Button("Ajouter compte");
        ajouterCompte.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                boolean ok = true;
                if(jEdit.getText().equals("")) {
                    jEdit.setBackground(Color.RED);
                    ok=false;
                } else {
                    jEdit.setBackground(Color.WHITE);
                }
                if(jEditTag.getText().equals("")) {
                    jEditTag.setBackground(Color.RED);
                    ok=false;
                } else {
                    jEditTag.setBackground(Color.WHITE);
                }

                if(ok){

                    LoginViewEnregistrer.this.ajouterUtilisateur(jEdit.getText(), jEditTag.getText(), Arrays.toString(passwordField1.getPassword()), avatar[0] != ""? avatar[0]: "C:\\Dev\\MessageApp\\src\\main\\resources\\images\\avatars\\img-defaut.png");
                }
            }
        });
        composant.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.EAST,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(jEdit, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(jLabelTag, new GridBagConstraints(0, 1, 1, 1, 0, 1, GridBagConstraints.EAST,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(jEditTag, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(jLabelImg, new GridBagConstraints(0, 2, 1, 1, 0, 1, GridBagConstraints.EAST,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(choisirImageBtn, new GridBagConstraints(1, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(jLabelMdp, new GridBagConstraints(0, 3, 1, 1, 0, 1, GridBagConstraints.EAST,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(passwordField1, new GridBagConstraints(1, 3, 1, 1, 1, 1, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, new Insets(5, 5, 0, 5), 0, 0));
        composant.add(ajouterCompte, new GridBagConstraints(0, 5, 2, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));

        this.add(labelVide, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));

        this.add(titreLabel, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
               GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));

        this.add(composant, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));

        this.add(labelVide2, new GridBagConstraints(2, 2, 1, 1, 1, 1, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(5, 5, 0, 5), 0, 0));
        //
        // Ajout des composants à la fenêtre


    }

    protected void ajouterUtilisateur(String nom, String tag, String mdp, String avatar) {
        System.out.println("Ajouter compte :" + nom + " - " + tag +" - " + mdp);
        for(ObserverLogin o: this.observerList) {
            o.notifyAddUser(nom, tag, mdp, avatar);
        }
    }

    private String choisirImage() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Choisir une image");

        FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", "jpg", "jpeg", "png", "gif");
        fileChooser.setFileFilter(filter);

        // Afficher la boîte de dialogue du JFileChooser
        int userSelection = fileChooser.showOpenDialog(this);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            // Récupérer le fichier sélectionné
            File selectedFile = fileChooser.getSelectedFile();

            // Définir le dossier de destination
            File destinationFolder = new File("C:\\Dev\\MessageApp\\src\\main\\resources\\images\\avatars");

            // Créer le dossier de destination s'il n'existe pas
            if (!destinationFolder.exists()) {
                destinationFolder.mkdirs();
            }

            // Copier le fichier vers le dossier de destination
            try {
                Path sourcePath = selectedFile.toPath();
                Path destinationPath = new File(destinationFolder, selectedFile.getName()).toPath();
                Files.copy(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);
                return destinationPath.toAbsolutePath().toString();
            } catch (IOException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, "Erreur lors du téléchargement de l'image", "Erreur", JOptionPane.ERROR_MESSAGE);
            }
        }
        return null;
    }
}
