package main.java.com.ubo.tp.message.ihm.login;

public interface UtilisateurFiltreObserver {
    public void notifyFiltreModifier();
}
