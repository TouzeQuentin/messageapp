package main.java.com.ubo.tp.message.ihm;

public interface ObserverMsg {
    void notifyAfficherMessageTagExistant();
    void notifyAfficherMessageEnregistrementOk();
    void notifyAfficherMessageConnexionOk();
    void notifyAfficherMessageConnexionKo();
    void notifyAfficherMessageDeconnexionOk();
}
