package main.java.com.ubo.tp.message.ihm;

import main.java.com.ubo.tp.message.ihm.login.LoginProfilView;
import main.java.com.ubo.tp.message.ihm.login.LoginUtilisateurView;
import main.java.com.ubo.tp.message.ihm.login.LoginViewConnexion;
import main.java.com.ubo.tp.message.ihm.login.LoginViewEnregistrer;
import main.java.com.ubo.tp.message.ihm.message.views.ListeMessageView;
import main.java.com.ubo.tp.message.ihm.message.views.MessageAffichageView;
import main.java.com.ubo.tp.message.ihm.message.views.MessageSaisieView;
import main.java.com.ubo.tp.message.ihm.session.Session;

import javax.swing.*;
import java.awt.*;
import java.util.UUID;

public class Navigation {

    protected LoginViewEnregistrer loginViewEnregistrer;
    protected LoginViewConnexion loginViewConnexion;
    protected MessageSaisieView messageSaisieView;
    protected MessageAffichageView messageAffichageView;
    protected ListeMessageView listeMessageView;
    protected LoginProfilView loginProfilView;
    protected LoginUtilisateurView loginUtilisateurView;
    protected JPanel panel;
    protected Session session;
    public Navigation(Session session) {
        this.session = session;
    }

    public void naviguerVersConnexion() {
        System.out.println("naviguer vers connexion");
        panel.removeAll();
        panel.add(loginViewConnexion, BorderLayout.CENTER);
        panel.repaint();
        panel.revalidate();
    }

    public void naviguerVersEnregistrement() {
        System.out.println("naviguer vers enregistrement");
        panel.removeAll();
        panel.add(loginViewEnregistrer, BorderLayout.CENTER);
        panel.repaint();
        panel.revalidate();
    }

    public void naviguerVersSaisieMessage() {
        System.out.println("naviguer vers saisie msg");
        if (session.getConnectedUser() == null) {
            this.naviguerVersConnexion();
            return;
        }
        panel.removeAll();
        panel.add(messageSaisieView, BorderLayout.CENTER);
        panel.repaint();
        panel.revalidate();
    }

    public void naviguerVersProfil(UUID id) {
        System.out.println("naviguer vers profil");
        if (session.getConnectedUser() == null) {
            this.naviguerVersConnexion();
            return;
        }
        panel.removeAll();
        loginProfilView.trouverUser(id);
        loginProfilView.afficherProfil();
        panel.add(loginProfilView, BorderLayout.CENTER);
        panel.repaint();
        panel.revalidate();
    }

    public void naviguerVersUtilisateurs() {
        System.out.println("naviguer vers les utilisateurs");
        if (session.getConnectedUser() == null) {
            this.naviguerVersConnexion();
            return;
        }
        panel.removeAll();
        loginUtilisateurView.afficherUtilisateur();
        panel.add(loginUtilisateurView, BorderLayout.CENTER);
        panel.repaint();
        panel.revalidate();
    }

    public void naviguerVersListeMessages() {
        System.out.println("naviguer vers liste des messages");
        if (session.getConnectedUser() == null) {
            this.naviguerVersConnexion();
            return;
        }
        panel.removeAll();
        listeMessageView.afficherMessage();
        panel.add(listeMessageView, BorderLayout.CENTER);

        panel.repaint();
        panel.revalidate();
    }

    public void setLoginViewEnregistrer(LoginViewEnregistrer loginViewEnregistrer) {
        this.loginViewEnregistrer = loginViewEnregistrer;
    }

    public void setLoginViewConnexion(LoginViewConnexion loginViewConnexion) {
        this.loginViewConnexion = loginViewConnexion;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public void setMessageSaisieView(MessageSaisieView messageSaisieView) {
        this.messageSaisieView = messageSaisieView;
    }

    public void setMessageAffichageView(MessageAffichageView messageAffichageView) {
        this.messageAffichageView = messageAffichageView;
    }

    public void setLoginProfilView(LoginProfilView loginProfilView) {
        this.loginProfilView = loginProfilView;
    }

    public void setLoginUtilisateurView(LoginUtilisateurView loginUtilisateurView) {
        this.loginUtilisateurView = loginUtilisateurView;
    }

    public void setListeMessageView(ListeMessageView listeMessageView) {
        this.listeMessageView = listeMessageView;
    }
}
