package main.java.com.ubo.tp.message.core.database;

import main.java.com.ubo.tp.message.datamodel.Message;
import main.java.com.ubo.tp.message.datamodel.User;

public class IDatabaseObserverImpl implements IDatabaseObserver{
    @Override
    public void notifyMessageAdded(Message addedMessage) {
        System.out.println("LOG: Message Added :" + addedMessage.getText());
    }

    @Override
    public void notifyMessageDeleted(Message deletedMessage) {

    }

    @Override
    public void notifyMessageModified(Message modifiedMessage) {

    }

    @Override
    public void notifyUserAdded(User addedUser) {
        System.out.println("LOG: User Added :" + addedUser.getUserTag());
    }

    @Override
    public void notifyUserDeleted(User deletedUser) {

    }

    @Override
    public void notifyUserModified(User modifiedUser) {

    }
}
